//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DatabaseServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_company
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_company()
        {
            this.tbl_stock = new HashSet<tbl_stock>();
        }
    
        public int company_id { get; set; }
        public string company_name { get; set; }
        public string contact_name { get; set; }
        public string address { get; set; }
        public string mobile { get; set; }
        public System.DateTime inserted_date { get; set; }
        public System.DateTime modified_date { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_stock> tbl_stock { get; set; }
    }
}
