﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using BusinessLayer.Interfaces;
using DatabaseServer;

namespace BusinessLayer.Implementation
{
    public class CompanyActions : CompanyInterface
    {
        private CompanyCoreEntity _compEntity;

        public CompanyActions()
        {
            _compEntity = null;
        }
        public CompanyActions(CompanyCoreEntity _comp)
        {
            _compEntity = _comp;
        }


        public CompanyCoreEntity AddCompanyDetails()
        {
           
            using (InventorySystemEntities entities = new InventorySystemEntities())
            {
                tbl_company _tblCompany = new tbl_company();

                _tblCompany.company_name = _compEntity.company_name;
                _tblCompany.contact_name = _compEntity.contact_name;
                _tblCompany.address = _compEntity.address;
                _tblCompany.mobile = _compEntity.mobile;
                _tblCompany.modified_date = DateTime.Now;
                _tblCompany.inserted_date = DateTime.Now;

                entities.tbl_company.Add(_tblCompany);
                entities.SaveChanges();
            }
            return _compEntity;
        }

        public CompanyCoreEntity UpdateCompanyDetails()
        {

            using (InventorySystemEntities entities = new InventorySystemEntities())
            {
                tbl_company _tblCompany = entities.tbl_company.First(filter => filter.company_id == _compEntity.company_id);

                _tblCompany.company_name = _compEntity.company_name;
                _tblCompany.contact_name = _compEntity.contact_name;
                _tblCompany.address = _compEntity.address;
                _tblCompany.mobile = _compEntity.mobile;
                _tblCompany.modified_date = DateTime.Now;
                entities.SaveChanges();
            }
            return _compEntity;
        }

        public CompanyCoreEntity DeleteCompanyDetails()
        {

            using (InventorySystemEntities entities = new InventorySystemEntities())
            {
                tbl_company _tblCompany = entities.tbl_company.First(filter => filter.company_id == _compEntity.company_id);

                entities.tbl_company.Remove(_tblCompany);
                entities.SaveChanges();
            }
            return _compEntity;
        }


    }
}
