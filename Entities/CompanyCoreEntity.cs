﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class CompanyCoreEntity 
    {
        public int company_id { get; set; }
        public string company_name { get; set; }
        public string contact_name { get; set; }
        public string address { get; set; }
        public string mobile { get; set; }
        public System.DateTime inserted_date { get; set; }
        public System.DateTime modified_date { get; set; }
    }
}
