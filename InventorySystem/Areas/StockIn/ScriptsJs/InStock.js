﻿
$(document).ready(function () {
    loadCompanyDetails();

    $("#addCompanyDetails").validate({
        messages: {
            company_name: "Company Name is required.",
            contact_name: "Contact Name is required.",
            address: "Address is required.",
            mobile: "Mobile is required."
        },
        focusInvalid: false,
        submitHandler: function (fields) {
            try {
                operationType = fields.btnSubmit[0].innerText;
                AjaxUrl = "/api/ServiceAPI/addCompanyDetails";
                successMessage = "Company details added successful";


                if (operationType != "Add") {
                    AjaxUrl = "/api/ServiceAPI/updateCompanyDetails";
                    successMessage = "Company details edited successful";
                }

                // ----------- Ajax call to save the data ----------- 
                var companyObject = {};
                companyObject.company_id = fields.company_id.value
                companyObject.company_name = fields.company_name.value
                companyObject.contact_name = fields.contact_name.value
                companyObject.address = fields.address.value
                companyObject.mobile = fields.mobile.value

                // Ajax call to webapi to save the comnpany details
                $.ajax({
                    type: "POST",
                    url: AjaxUrl,
                    data: JSON.stringify(companyObject),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (data, textStatus, xhr) {
                        swal("Good job!", successMessage, "success")
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        try {
                            var errorMessage = "";
                            responseText = jQuery.parseJSON(xhr.responseText);
                            errorMessage += "<div><b>" + errorType + " " + exception + "</b></div>";
                            errorMessage += "<div><u>Exception</u>:<br /><br />" + responseText.ExceptionType + "</div>";
                            errorMessage += "<div><u>StackTrace</u>:<br /><br />" + responseText.StackTrace + "</div>";
                            errorMessage += "<div><u>Message</u>:<br /><br />" + responseText.Message + "</div>";
                            alert(errorMessage);
                            console.log(errorMessage);
                        } catch (e) {
                            responseText = xhr.responseText;
                            alert(responseText);
                            console.log(responseText);
                        }
                    }
                });
            } catch (e) {
                console.log("Error in saving Company Details : ", e);
            }
            finally {
                // Reset the form to add new enteries
                document.getElementById("addCompanyDetails").reset();

                // Reload the table to view the newly added company
                loadCompanyDetails();

                // hide the bootstrap modal on successfull add of company to database
                $('#addCompanyModel').modal('hide');
            }
        }
    });

    // To reload the list of companies in the table
    $(".refresh-company").click(function () {
        loadCompanyDetails();
    });
});

function loadCompanyDetails() {
    try {
        $('.tbl_view_company_in_stock').DataTable({
            "ajax": {
                "url": "/api/ServiceAPI/GetCompanyView",
                "dataSrc": ""
            },
            "columns": [
                        { data: "company_id", "visible": false, "searchable": false },
                        { data: "company_name" },
                        { data: "contact_name" },
                        { data: "address" },
                        { data: "mobile" },
                        {
                            render: function (data, type, row) {
                                return "<span class='action-icons'> <span class='label label-info' onclick='editCompanyDetails(" + JSON.stringify(row) + ")'><i class='fa fa-pencil-square-o fa-2x'></i></span>" +
                                " <span class='label label-warning' onclick='deleteCompanyDetails(" + JSON.stringify(row) + ")'><i class='fa fa-trash-o fa-2x'></i></span></span>";
                            }
                        }
            ],
            destroy: true
        });
    } catch (e) {
        console.log(e);
    }
}

function editCompanyDetails(company_info) {
    try {
        $('#addCompanyModel').modal();
        $(".modelTitle").html("Edit Company");
        $("#btnSubmit").html("Edit");

        $("#companyId").val(company_info.company_id);
        $("#companyName").val(company_info.company_name);
        $("#ctntName").val(company_info.contact_name);
        $("#address").val(company_info.address);
        $("#mobile").val(company_info.mobile);
    } catch (e) {
        console.log("Error - editCompanyDetails : ", e);
    }
}

function deleteCompanyDetails(company_info) {
    try {
        AjaxUrl = "/api/ServiceAPI/deleteCompanyDetails";

        swal({
            title: "Are you sure?",
            text: "Company details will be removed parmanently!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            // Ajax call to webapi to save the comnpany details
            $.ajax({
                type: "POST",
                url: AjaxUrl,
                data: JSON.stringify(company_info),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, textStatus, xhr) {
                    if (data) {
                        // Reload the table to view the newly added company
                        loadCompanyDetails();

                        swal("Deleted!", "Company details are removed successful", "success");
                    }
                    else {
                        // Reload the table to view the newly added company
                        loadCompanyDetails();

                        swal("Cancelled", "Company details are not removed", "error");
                    }
                    
                },
                error: function (xhr, textStatus, errorThrown) {
                    try {
                        var errorMessage = "";
                        responseText = jQuery.parseJSON(xhr.responseText);
                        errorMessage += "<div><b>" + errorType + " " + exception + "</b></div>";
                        errorMessage += "<div><u>Exception</u>:<br /><br />" + responseText.ExceptionType + "</div>";
                        errorMessage += "<div><u>StackTrace</u>:<br /><br />" + responseText.StackTrace + "</div>";
                        errorMessage += "<div><u>Message</u>:<br /><br />" + responseText.Message + "</div>";
                        alert(errorMessage);
                        console.log(errorMessage);
                    } catch (e) {
                        responseText = xhr.responseText;
                        alert(responseText);
                        console.log(responseText);
                    }
                }
            });
            
        });





        
    } catch (e) {
        console.log("Error - editCompanyDetails : ", e);
    }
}