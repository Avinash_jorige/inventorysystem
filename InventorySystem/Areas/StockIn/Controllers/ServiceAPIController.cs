﻿using BusinessLayer.Implementation;
using DatabaseServer;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InventorySystem.Areas.StockIn.Controllers
{
    public class ServiceAPIController : ApiController
    {
        public HttpResponseMessage GetCompanyView()
        {
            List<tbl_company> allCompany = new List<tbl_company>();

            using (InventorySystemEntities dc = new InventorySystemEntities())
            {
                allCompany = dc.tbl_company.OrderBy(a => a.modified_date).ToList();
                HttpResponseMessage response;
                response = Request.CreateResponse(HttpStatusCode.OK, allCompany);
                return response; 
            }
        }

        [Route("api/ServiceAPI/addCompanyDetails")]
        [HttpPost]
        public bool AddCompany(CompanyCoreEntity _comp)
        {
            if (_comp != null)
            {
                CompanyActions _ca = new CompanyActions(_comp);
                _ca.AddCompanyDetails();
                return true;
            }
            return false;
        }

        [Route("api/ServiceAPI/updateCompanyDetails")]
        [HttpPost]
        public bool UpdateCompany(CompanyCoreEntity _comp)
        {
            if (_comp != null)
            {
                CompanyActions _ca = new CompanyActions(_comp);
                _ca.UpdateCompanyDetails();
                return true;
            }
            return false;
        }

        [Route("api/ServiceAPI/deleteCompanyDetails")]
        [HttpPost]
        public bool DeleteCompany(CompanyCoreEntity _comp)
        {
            if (_comp != null)
            {
                CompanyActions _ca = new CompanyActions(_comp);
                _ca.DeleteCompanyDetails();
                return true;
            }
            return false;
        }


    }
}
