﻿using System.Web;
using System.Web.Optimization;

namespace InventorySystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //script files
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/jquery-1.10.2.min.js",
                        "~/Scripts/jquery.validate.min.js",
                        "~/Content/js/bootstrap.min.js",
                        "~/Scripts/datatables.min.js",
                        "~/Content/js/sweetalert.min.js"
                        ));

            // style sheet files
            bundles.Add(new StyleBundle("~/Content/styles").Include(
                        "~/Content/css/bootstrap.min.css",
                        "~/Content/font-awesome/css/font-awesome.min.css",
                        "~/Content/css/sweetalert.css",
                        "~/Content/Site.css"
                     ));
        }
    }
}
